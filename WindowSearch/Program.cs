﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WindowSearch
{
    class Program
    {
        // delegate void Del(Image<Gray, float> img, int height, int width, int startPosition, Color col, Color col2, out int[,] array);

        static string pathToFolder = @"D:\temp\WindowSearch";
        static void Main(string[] args)
        {

            var img = new Image<Rgb, byte>($@"{pathToFolder}\1\ImageToScan.jpg");
            Console.WriteLine("Upload first image");

            var windowsName = GetWindowsNameByImage(img);

            var tmpArray = GetTemplatsByWindowsName(windowsName);
            Console.WriteLine("Upload templates");

            var maskArray = GetMasksByWindowsName(windowsName);
            Console.WriteLine("Upload mask");

            var res = new Image<Rgb, float>(img.Size.Width, img.Size.Height);
            CvInvoke.MatchTemplate(img, tmpArray[0], res, TemplateMatchingType.Sqdiff);
            var OA = res.GetOutputArray();
            var mat = OA.GetUMat();
            var res2 = mat.ToImage<Gray, float>();
            res2.Bitmap.Save($@"{pathToFolder}\Results\newImage3.jpg");

            var bitmap2 = new Bitmap(res2.Bitmap);
            Console.WriteLine("Step 1");
            Color col = Color.Black;

            var array2 = new int[res2.Height, res2.Width];
            for (int i = 0; i < 1030; i += 103)
            {
                ChangeImage(res2, 103, res2.Width, i, col.R + col.G + col.B + 100, out int[,] array);
                for (int j = 0; j < 103; j++)
                {
                    for (int k = 0; k < res2.Width; k++)
                    {
                        if (array[k, j] == 255)
                        {
                            bitmap2.SetPixel(k, i + j, Color.White);
                        }
                    }
                }
            }
            Console.WriteLine("Step 2");

            Console.WriteLine("Step 3");
            bitmap2.Save($@"{pathToFolder}\Results\newImage.jpg");
            ShowMinMax(res);

            Console.Read();
        }
        static string GetWindowsNameByImage(Image<Rgb, byte> img)
        {
            return "Win_8_1";
        }
        static Image<Rgb, byte>[] GetTemplatsByWindowsName(string windowsName)
        {
            int count = 4;
            var imageArray = new Image<Rgb, byte>[count];
            imageArray[0] = new Image<Rgb, byte>($@"{pathToFolder}\{windowsName}\Head_active.jpg");
            imageArray[1] = new Image<Rgb, byte>($@"{pathToFolder}\{windowsName}\Frame_active.jpg");
            //imageArray[2] = new Image<Rgb, byte>($@"{pathToFolder}\{windowsName}\Head_inactive.jpg");
            //imageArray[3] = new Image<Rgb, byte>($@"{pathToFolder}\{windowsName}\Frame_inactive.jpg");
            return imageArray;
        }
        static Image<Rgb, byte>[] GetMasksByWindowsName(string windowsName)
        {
            int count = 2;
            var imageArray = new Image<Rgb, byte>[count];
            //imageArray[0] = new Image<Rgb, byte>($@"{pathToFolder}\{windowsName}\Head_active_mask.jpg");
            // imageArray[1] = new Image<Rgb, byte>($@"{pathToFolder}\{windowsName}\Frame_active_mask.jpg");
            return imageArray;

        }

        static void ShowMinMax(Image<Rgb, float> res)
        {
            double[] d1 = new double[10];
            double[] d2 = new double[10];
            Point[] p1 = new Point[10];
            Point[] p2 = new Point[10];
            res.MinMax(out d1, out d2, out p1, out p2);
            for (int i = 0; i < d1.Length; i++)
            {
                Console.WriteLine($"min #{i} ={d1[i]}, P={p1[i]}");
            }
            for (int i = 0; i < d2.Length; i++)
            {
                Console.WriteLine($"max #{i} ={d2[i]}, P={p2[i]}");
            }

        }
        static void ChangeImage(Image<Gray, float> img, int height, int width, int startPosition, int colorBlack, out int[,] array)
        {
            Color col2;
            int i, j;
            int step = 5;
            array = new int[width, height];
            BitmapToIntArray(img, out int[] array1);

            for (i = 0; i < height; i += step)
            {
                for (j = 0; j < img.Width; j += step)
                {
                    array[j, i] = array1[i * width + j];
                    // byte[] bytes = BitConverter.GetBytes(array[j, i]);

                    // Console.Write(Color.FromArgb(bytes[3], bytes[2], bytes[1], bytes[0]) + " ");
                }
                //  Console.WriteLine("");
            }
            for (i = 0; i < height; i += step)
            {
                for (j = 0; j < img.Width; j += step)
                {
                    byte[] bytes = BitConverter.GetBytes(array[j, i]);
                    col2 = Color.FromArgb(bytes[3], bytes[2], bytes[1], bytes[0]);
                    if (col2.R + col2.G + col2.B > colorBlack + 100)
                    {
                        array[j, i] = 255;
                    }
                    else
                    {
                        array[j, i] = 0;
                    }
                    GC.Collect();
                }
                Console.WriteLine($"Step 1_{i + startPosition}");
                GC.Collect();
            }
        }
        static void BitmapToIntArray(Image<Gray, float> img, out int[] array)
        {
            int w = img.Width, h = img.Height;
            array = new int[w * h];
            const int PixelWidth = 3;
            const PixelFormat PixelFormat = PixelFormat.Format24bppRgb;
            Bitmap image = new Bitmap(w, h);
            //Bitmap image = new Bitmap(img.Bitmap);
            image.Save($@"{pathToFolder}\Results\semi.jpg");
            int i = 0, j = 0;
            /*byte[] bytes = new byte[img.Bytes.Length];
            foreach (var b in img.Bytes)
            {
                bytes[i] = b;
                i++;
            }
            i = 0;
            for (int b = 0; b <bytes.Length; b += PixelWidth )
            {
                if (i < h)
                {
                    if (j < w)
                    {
                        image.SetPixel(j, i, Color.FromArgb(bytes[b + 3], bytes[b + 2], bytes[b + 1], bytes[b + 0]));
                        //Console.WriteLine($"{i}_{j}");
                        j++;
                    }
                    else
                    {
                        Console.WriteLine($"{i}");
                        i ++;
                        j = 0;
                    }
                }
                else
                {
                    image.Save($@"{pathToFolder}\Results\semi2.jpg");
                }
            }*/
           // Console.WriteLine("Semi 2");
          //  image.Save($@"{pathToFolder}\Results\semi2.jpg");
            /*
            int startX = 0;
            int startY = 0;
            int offset=0;
            int scansize = w;
            // En garde!
            if (image == null) throw new ArgumentNullException("image");
            if (array == null) throw new ArgumentNullException("rgbArray");
            if (startX < 0 || startX + w > image.Width) throw new ArgumentOutOfRangeException("startX");
            if (startY < 0 || startY + h > image.Height) throw new ArgumentOutOfRangeException("startY");
            if (w < 0 || w > scansize || w > image.Width) throw new ArgumentOutOfRangeException("w");
            if (h < 0 || (array.Length < offset + h * scansize) || h > image.Height) throw new ArgumentOutOfRangeException("h");

            BitmapData data = image.LockBits(new Rectangle(startX, startY, w, h), ImageLockMode.ReadOnly, PixelFormat);
            try
            {
                byte[] pixelData = new Byte[data.Stride];
                for (int scanline = 0; scanline < data.Height; scanline++)
                {
                    Marshal.Copy(data.Scan0 + (scanline * data.Stride), pixelData, 0, data.Stride);
                    for (int pixeloffset = 0; pixeloffset < data.Width; pixeloffset++)
                    {
                       array[offset + (scanline * scansize) + pixeloffset] =
                            (pixelData[pixeloffset * PixelWidth + 2] << 16) +   // R 
                            (pixelData[pixeloffset * PixelWidth + 1] << 8) +    // G
                            pixelData[pixeloffset * PixelWidth];                // B
                    }
                }
            }
            finally
            {
                image.UnlockBits(data);
            }
            */
        }
    }
}
