﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Images
{
    public class ImageManager
    {
        public Image<Rgb, byte> Image { get; set; }
        public Image<Rgb, byte> Template { get; set; }
        public Image<Rgb, byte> Mask { get; set; }
        public Image<Gray, float> Result { get; set; }
        private string pathToFolder = @"D:\temp\WindowSearch";

        public ImageManager()
        {
            Image = new Image<Rgb, byte>($@"{pathToFolder}\1\ImageToScan.jpg");
            Console.WriteLine("Upload first image");

            Template = new Image<Rgb, byte>($"{pathToFolder}MyShortCut.jpg");
            Console.WriteLine("Upload second image");

            Mask = new Image<Rgb, byte>($"{pathToFolder}MyMask2.jpg");
            Console.WriteLine("Upload mask");

            Result = new Image<Gray, float>(Image.Size.Width, Image.Size.Height);
        }
        public ImageManager(string imgPath, string tmpPath, string maskPath, string resPath) { }
        public void MatchTemplate()
        {
            CvInvoke.MatchTemplate(Image, Template, Result, TemplateMatchingType.Sqdiff, Mask);
            Result = Result.GetOutputArray().GetUMat().ToImage<Gray, float>();
        }
        Image<Rgb, byte>[] GetTemplatsByWindowsName(string windowsName)
        {
            int count = 4;
            var imageArray = new Image<Rgb, byte>[count];
            imageArray[0] = new Image<Rgb, byte>($@"{pathToFolder}\{windowsName}\Head_active.jpg");
            imageArray[1] = new Image<Rgb, byte>($@"{pathToFolder}\{windowsName}\Frame_active.jpg");
            //imageArray[2] = new Image<Rgb, byte>($@"{pathToFolder}\{windowsName}\Head_inactive.jpg");
            //imageArray[3] = new Image<Rgb, byte>($@"{pathToFolder}\{windowsName}\Frame_inactive.jpg");
            return imageArray;
        }
        Image<Rgb, byte>[] GetMasksByWindowsName(string windowsName)
        {
            int count = 2;
            var imageArray = new Image<Rgb, byte>[count];
            //imageArray[0] = new Image<Rgb, byte>($@"{pathToFolder}\{windowsName}\Head_active_mask.jpg");
            // imageArray[1] = new Image<Rgb, byte>($@"{pathToFolder}\{windowsName}\Frame_active_mask.jpg");
            return imageArray;

        }

    }
}
