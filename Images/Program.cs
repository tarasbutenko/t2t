﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Images
{
    class Program
    {
        static void Main(string[] args)
        {


            var res = new Image<Rgb, float>(img.Size.Width, img.Size.Height);

            CvInvoke.MatchTemplate(img, tmp, res, TemplateMatchingType.Sqdiff);
            Console.WriteLine("Done!");
            img.Dispose();
            tmp.Dispose();
            mask.Dispose();
            res.Dispose();
            Console.Read();
        }
    }
}
